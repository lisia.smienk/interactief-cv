package com.ICV.dto;

import com.ICV.domein.Kwartetspeler;

public class KwartetspelerDto {
	private Long id;
	private String naam;
	private int aantalKaarten;
	private int aantalKwartetten;
	
	public KwartetspelerDto() {}
	
	public KwartetspelerDto(Kwartetspeler s) {
		id = s.getId();
		naam = s.getNaam();
		aantalKaarten = s.aantalKaarten();
		aantalKwartetten = s.aantalKwartetten();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public int getAantalKaarten() {
		return aantalKaarten;
	}

	public void setAantalKaarten(int aantalKaarten) {
		this.aantalKaarten = aantalKaarten;
	}
	public int getAantalKwartetten() {
		return aantalKwartetten;
	}

	public void setAantalKwartetten(int aantalKwartetten) {
		this.aantalKwartetten = aantalKwartetten;
	}
}
