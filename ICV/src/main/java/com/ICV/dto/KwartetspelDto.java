package com.ICV.dto;

import com.ICV.domein.Kwartetspel;

public class KwartetspelDto {
	private Long id;
	private String naam;
	private Long potid;
	private KwartetspelerDto actieveSpeler;
	private String gamelog;
	private Boolean status;

	public KwartetspelDto() {}

	public KwartetspelDto(Long id) {
		this.id = id;
	}	

	public KwartetspelDto(Kwartetspel spel) {
		id = spel.getId();
		naam = spel.getNaam();
		potid = spel.getKwartetpot().getId();
		actieveSpeler = new KwartetspelerDto(spel.getActieveSpeler());
		gamelog =  spel.getGamelog();
		status = spel.afgelopen();
		if(status) {
			String log = "Het spel is afgelopen!*****+";
			switch(spel.eindscore()) {
			case -1: log += "Je hebt verloren..!*****"; break;
			case 0: log += "Je hebt gelijkt gespeeld.*****"; break;
			case 1: log += "Je hebt verloren...*****"; break;
			}
			gamelog = log;
		}
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPotid() {
		return potid;
	}

	public void setPotid(Long potid) {
		this.potid = potid;
	}
	public KwartetspelerDto getActieveSpeler() {
		return actieveSpeler;
	}
	public void setActieveSpeler(KwartetspelerDto actieveSpeler) {
		this.actieveSpeler = actieveSpeler;
	}
	public String getGamelog() {
		return gamelog;
	}
	public void setGamelog(String gamelog) {
		this.gamelog = gamelog;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
}
