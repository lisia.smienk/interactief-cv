package com.ICV.dto;

import com.ICV.domein.Kwartetkaart;

public class KwartetkaartDto {
	private Long id;
	private String titel;
	private String beschrijving;
	
	public KwartetkaartDto(Long id, String titel, String beschrijving) {
		this.id = id;
		this.titel = titel;
		this.beschrijving = beschrijving;
	}
	
	public KwartetkaartDto(Kwartetkaart k) {
		id = k.getId();
		titel = k.getTitel();
		beschrijving = k.getBeschrijving();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}
	public String getBeschrijving() {
		return beschrijving;
	}
	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}
}
