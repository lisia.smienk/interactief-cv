package com.ICV.dto;

public class KwartetKaartKoppelingDto {
	private Long kwartetid;
	private Long kwartetkaartid;
	
	public Long getKwartetid() {
		return kwartetid;
	}
	public void setKwartetid(Long kwartetid) {
		this.kwartetid = kwartetid;
	}
	public Long getKwartetkaartid() {
		return kwartetkaartid;
	}
	public void setKwartetkaartid(Long kwartetkaartid) {
		this.kwartetkaartid = kwartetkaartid;
	}	
}