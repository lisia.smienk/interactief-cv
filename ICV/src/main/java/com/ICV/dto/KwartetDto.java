package com.ICV.dto;

import com.ICV.domein.Kwartet;

public class KwartetDto {
	private Long id;
	private String categorie;
	private String kleur;
	
	public KwartetDto(Long id, String categorie, String kleur) {
		this.id = id;
		this.categorie = categorie;
		this.kleur = kleur;
	}
	
	public KwartetDto(Kwartet k) {
		this.id = k.getId();
		this.categorie = k.getCategorie();
		this.kleur = k.getKleur();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getKleur() {
		return kleur;
	}
	public void setKleur(String kleur) {
		this.kleur = kleur;
	}
}
