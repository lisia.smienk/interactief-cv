package com.ICV.dto;

public class KwartetpotDto {
	private Long id;
	private int size;
	
	public KwartetpotDto() {}

	public KwartetpotDto(Long id, int size) {
		this.id = id;
		this.size = size;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
