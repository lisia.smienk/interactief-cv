package com.ICV.domein;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Kwartetspeler {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String naam;
	@ManyToMany
	List<Kwartet> kwartetten;
	@ManyToMany
	List<Kwartetkaart> kwartetkaarten;
	@ManyToOne
	Kwartetspel spel;
	
	//TODO: Wanneer het toevoegen van een kaart aan kaarten leidt tot
	//voltooien van een kwartet, deze meteen toevoegen aan kwartetten en verwijderen uit hand
	
	public Kwartetspeler(){}
	
	public Kwartetspeler(String naam) {
		this();
		this.naam = naam;
		this.kwartetten = new ArrayList<Kwartet>();
		this.kwartetkaarten = new ArrayList<Kwartetkaart>();
	}
	
	//TODO: Unit testing
	//TODO: Efficiënte code?
	public List<Kwartetkaart> teVragenKaarten(){
		List<Kwartetkaart> kaarten = new ArrayList<>();
		for(Kwartetkaart k : kwartetkaarten) {
			for(Kwartetkaart subkaart : k.getKwartet().getKaarten()) {
				if(!kaarten.contains(subkaart) && !kwartetkaarten.contains(subkaart)) {
					kaarten.add(subkaart);
				}
			}
		}
		return kaarten;
	}
	
	public void voegKaartToe(Kwartetkaart k) {
		kwartetkaarten.add(k);
		k.voegToeAanSpeler(this);
		Kwartet kwartet = k.getKwartet();
		if(kwartetCompleet(kwartet)) {
			verwijderKwartetUitHand(kwartet);
			voegKwartetToe(kwartet);
		}
	}
	
	private void verwijderKwartetUitHand(Kwartet k) {
		kwartetkaarten.removeIf(kwartetkaart -> kwartetkaart.getKwartet().equals(k));
		for(Kwartetkaart kaart : k.getKaarten()) {
			kaart.verwijderVanSpeler(this);
		}
	}
	
	private boolean kwartetCompleet(Kwartet k) {
		boolean compleet = true;
		for(Kwartetkaart kaart : k.getKwartetkaarten()) {
			compleet = compleet && kwartetkaarten.contains(kaart);
			if(!compleet) {
				return false;
			}
		}
		return compleet;
	}
	
	public void verwijderKaart(Kwartetkaart k) {
		if(kwartetkaarten.contains(k)) {
			kwartetkaarten.remove(k);
			k.verwijderVanSpeler(this);
		}
	}

	public void voegKwartetToe(Kwartet k) {
		kwartetten.add(k);
		k.voegKwartetspelerToe(this);
		//TODO: Heroverwegen dubbelzichtige relatie?
	}
	
	public int aantalKwartetten() {
		return kwartetten.size();
	}
	
	public int aantalKaarten() {
		return kwartetkaarten.size();
	}
	
	private void sorteerKaarten() {
		//Sorteert op basis van bij welk kwartet het hoort??? Even checken
		//TODO: Unit testing
		Collections.sort(kwartetkaarten);
	}
	
	public boolean geenKaarten() {
		return kwartetkaarten.size() == 0;
	}
	
	//GETTERS EN SETTERS -----------------------------------------------
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Kwartet> getKwartetten() {
		return kwartetten;
	}
	public void setKwartetten(List<Kwartet> kwartetten) {
		this.kwartetten = kwartetten;
	}
	public List<Kwartetkaart> getKaarten() {
		sorteerKaarten();
		return kwartetkaarten;
	}
	public void setKaarten(List<Kwartetkaart> kaarten) {
		this.kwartetkaarten = kaarten;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public List<Kwartetkaart> getKwartetkaarten() {
		return kwartetkaarten;
	}
	public void setKwartetkaarten(List<Kwartetkaart> kwartetkaarten) {
		this.kwartetkaarten = kwartetkaarten;
	}

	public Kwartetspel getSpel() {
		return spel;
	}

	public void setSpel(Kwartetspel spel) {
		this.spel = spel;
	}
}
