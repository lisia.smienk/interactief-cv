package com.ICV.domein;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Kwartetkaart implements Comparable<Kwartetkaart>{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String titel;
	private String beschrijving;
	@ManyToMany (cascade = CascadeType.ALL, mappedBy = "kwartetkaarten")
	private List<Kwartetspeler> kwartetspelers;
	@ManyToOne
	private Kwartet kwartet;
	
	public void voegToeAanSpeler(Kwartetspeler s) {
		kwartetspelers.add(s);
	}
	
	public void verwijderVanSpeler(Kwartetspeler s) {
		kwartetspelers.remove(s);
	}
	

	@Override
	public int compareTo(Kwartetkaart o) {
		return id.compareTo(o.id);
	}
	
	//GETTERS AND SETTERS------------------------------------------------------
	public Kwartet getKwartet() {
		return kwartet;
	}	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}
	public String getBeschrijving() {
		return beschrijving;
	}
	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}
	public List<Kwartetspeler> getKwartetspelers() {
		return kwartetspelers;
	}
	public void setKwartetspelers(List<Kwartetspeler> kwartetspelers) {
		this.kwartetspelers = kwartetspelers;
	}
	public void setKwartet(Kwartet kwartet) {
		this.kwartet = kwartet;
	}
}
