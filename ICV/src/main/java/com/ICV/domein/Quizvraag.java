package com.ICV.domein;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Quizvraag {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int volgnummer;
	private String vraag;
	private String[] antwoorden;
	private int indexLisia;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getVolgnummer() {
		return volgnummer;
	}
	public void setVolgnummer(int volgnummer) {
		this.volgnummer = volgnummer;
	}
	public String getVraag() {
		return vraag;
	}
	public void setVraag(String vraag) {
		this.vraag = vraag;
	}
	public String[] getAntwoorden() {
		return antwoorden;
	}
	public void setAntwoorden(String[] antwoorden) {
		this.antwoorden = antwoorden;
	}
	public int getIndexLisia() {
		return indexLisia;
	}
	public void setIndexLisia(int indexLisia) {
		this.indexLisia = indexLisia;
	}
}
