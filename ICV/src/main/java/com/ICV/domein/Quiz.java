package com.ICV.domein;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Quiz {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int huidigeVraag;
	private int aantalOvereenkomstig;
	private String naam;
	private long aantalVragen;
	
	public void updateAantalOvereenkomstig(boolean goed) {
		if(goed) {
			aantalOvereenkomstig ++;
		}
	}
	
	public void volgendeVraag() {
		huidigeVraag ++;
	}
	
	/* -----------------------------------------------
	 * GETTERS EN SETTERS
	 */
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getHuidigeVraag() {
		return huidigeVraag;
	}
	public void setHuidigeVraag(int huidigeVraag) {
		this.huidigeVraag = huidigeVraag;
	}
	public int getAantalOvereenkomstig() {
		return aantalOvereenkomstig;
	}
	public void setAantalOvereenkomstig(int aantalOvereenkomstig) {
		this.aantalOvereenkomstig = aantalOvereenkomstig;
	}
	public long getAantalVragen() {
		return aantalVragen;
	}
	public void setAantalVragen(long aantalVragen) {
		this.aantalVragen = aantalVragen;
	}
}
