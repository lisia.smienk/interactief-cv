package com.ICV.domein;

import static com.ICV.IcvApplication.random;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Kwartetspel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String naam;
	private String gamelog;
	@OneToOne
	private Kwartetspeler actieveSpeler;
	@OneToOne
	private Kwartetpot kwartetpot;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Kwartetspeler> kwartetspelers;

	public void deelKaarten() {
		for(int i=0; i<9; i++) {
			Kwartetkaart k = kwartetpot.pakKaart();

			kwartetspelers.get(i%3).kwartetkaarten.add(k);
			k.voegToeAanSpeler(kwartetspelers.get(i%3));			
		}
	}

	public void voegSpelersToe(List<Kwartetspeler> spelers) {
		kwartetspelers = spelers;
		for(Kwartetspeler s : kwartetspelers) {
			s.setSpel(this);
			if(s.getNaam().equals("jou")) {
				setActieveSpeler(s);
				return;
			}
		}
	}

	public boolean verwerkenVraag(Kwartetkaart gevraagdeKaart, Kwartetspeler bevraagdeSpeler) {
		if(bevraagdeSpeler.getKwartetkaarten().contains(gevraagdeKaart)) {
			actieveSpeler.voegKaartToe(gevraagdeKaart);
			bevraagdeSpeler.verwijderKaart(gevraagdeKaart);
			return true;
		} else {
			pakKaart();
			return false;
		}
	}

	public void pakKaart() {
		Kwartetkaart k = kwartetpot.pakKaart();
		if(k != null) {
			actieveSpeler.voegKaartToe(k);
		}
		volgendeSpeler();
	}

	private void setVolgendeSpeler(String spelernaam) {
		for(Kwartetspeler speler : kwartetspelers) {
			if(speler.getNaam().equals(spelernaam)) {
				actieveSpeler = speler;
				return;
			}
		}
	}

	public void volgendeSpeler() {
		switch(actieveSpeler.getNaam()) {
		case "Lisia":
			setVolgendeSpeler("Peter");
			break;
		case "Peter":
			setVolgendeSpeler("jou");
			break;
		case "jou":
			setVolgendeSpeler("Lisia");
			break;
		}
	}

	public boolean afgelopen() {
		boolean result = true;
		for(Kwartetspeler speler: kwartetspelers) {
			result = result && (speler.aantalKaarten() == 0);
		}
		return result;
	}

	public int eindscore() {
		int aantalLisia = 0, aantalPeter = 0, aantalBezoeker = 0;
		for(Kwartetspeler s: kwartetspelers) {
			switch(s.getNaam()) {
			case "Lisia": aantalLisia = s.aantalKwartetten(); break;
			case "Peter": aantalPeter = s.aantalKwartetten(); break;
			case "jou": aantalBezoeker = s.aantalKwartetten(); break; 
			}
		}
		if(aantalBezoeker > aantalLisia) {
			if(aantalBezoeker > aantalPeter) {
				return 1;
			} else {
				if(aantalBezoeker == aantalPeter) {
					return 0;
				} else {
					return -1;
				}
			}
		} else if(aantalBezoeker == aantalLisia) {
			if(aantalBezoeker >= aantalPeter) {
				return 0;
			} else {
				return -1;
			}
		} else {
			return -1;
		}
	}

	public void trekRandomKaartVan(Kwartetspeler s) {
		Kwartetkaart k = s.getKwartetkaarten().get(random.nextInt(s.getKwartetkaarten().size()));
		actieveSpeler.voegKaartToe(k);
		s.verwijderKaart(k);
	}

	public void voegToeAanGameLog(String log) {
		gamelog += log;
	}

	public void clearGameLog() {
		gamelog = "";
	}

	//GETTERS AND SETTERS------------------------------------------------------
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Kwartetspeler> getKwartetspelers() {
		return kwartetspelers;
	}
	public void setKwartetspelers(List<Kwartetspeler> kwartetspelers) {
		this.kwartetspelers = kwartetspelers;
	}
	public Kwartetpot getKwartetpot() {
		return kwartetpot;
	}
	public void setKwartetpot(Kwartetpot kwartetpot) {
		this.kwartetpot = kwartetpot;
	}
	public Kwartetspeler getActieveSpeler() {
		return actieveSpeler;
	}
	public void setActieveSpeler(Kwartetspeler actieveSpeler) {
		this.actieveSpeler = actieveSpeler;
	}
	public String getGamelog() {
		return gamelog;
	}
	public void setGamelog(String gamelog) {
		this.gamelog = gamelog;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
}