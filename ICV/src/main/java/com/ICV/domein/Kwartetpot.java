package com.ICV.domein;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import static com.ICV.IcvApplication.random;

@Entity
public class Kwartetpot {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	private Kwartetspel kwartetspel;

	@ManyToMany(cascade = CascadeType.ALL)
	private List<Kwartetkaart> kwartetkaarten;

	public Kwartetpot() {}
	
	public Kwartetpot(List<Kwartetkaart> kaarten){
		kwartetkaarten = kaarten;
	}
	
	public Kwartetkaart pakKaart() {
		if(kwartetkaarten.size() > 0) {
			int rand = random.nextInt(kwartetkaarten.size());
			Kwartetkaart k = kwartetkaarten.get(rand);
			kwartetkaarten.remove(k);
			return k;
		} else {
			return null;
		}
	}

	public void voegKaartToe(Kwartetkaart k) {
		kwartetkaarten.add(k);
	}
	
	public boolean isLeeg() {
		return kwartetkaarten.size() == 0;
	}
	
	//GETTERS EN SETTERS ----------------------------------------------------------
	public List<Kwartetkaart> getKwartetkaarten() {
		return kwartetkaarten;
	}
	public void setKwartetkaarten(List<Kwartetkaart> kwartetkaarten) {
		this.kwartetkaarten = kwartetkaarten;
	}
	public Kwartetspel getKwartetspel() {
		return kwartetspel;
	}
	public void setKwartetspel(Kwartetspel kwartetspel) {
		this.kwartetspel = kwartetspel;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
}