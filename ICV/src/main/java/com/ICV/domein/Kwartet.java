package com.ICV.domein;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Kwartet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String categorie;
	private String kleur;
	@OneToMany
	private List<Kwartetkaart> kwartetkaarten;
	@ManyToMany (cascade = CascadeType.ALL, mappedBy = "kwartetten")
	private List<Kwartetspeler> kwartetspelers;

	public void voegKwartetkaartToe(Kwartetkaart k) {
		kwartetkaarten.add(k);
	}
	
	public void voegKwartetspelerToe(Kwartetspeler s) {
		kwartetspelers.add(s);
	}
	
	public void verwijderKwartetspeler(Kwartetspeler s) {
		kwartetspelers.remove(s);
	}
	
	//GETTERS AND SETTERS------------------------------------------------------
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public List<Kwartetkaart> getKaarten() {
		return kwartetkaarten;
	}

	public void setKaarten(List<Kwartetkaart> kaarten) {
		this.kwartetkaarten = kaarten;
	}

	public List<Kwartetkaart> getKwartetkaarten() {
		return kwartetkaarten;
	}

	public void setKwartetkaarten(List<Kwartetkaart> kwartetkaarten) {
		this.kwartetkaarten = kwartetkaarten;
	}

	public List<Kwartetspeler> getKwartetspelers() {
		return kwartetspelers;
	}

	public void setKwartetspelers(List<Kwartetspeler> kwartetspelers) {
		this.kwartetspelers = kwartetspelers;
	}

	public String getKleur() {
		return kleur;
	}

	public void setKleur(String kleur) {
		this.kleur = kleur;
	}
}
