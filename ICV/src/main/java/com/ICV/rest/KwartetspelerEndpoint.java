package com.ICV.rest;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.KwartetkaartService;
import com.ICV.controller.KwartetspelerService;
import com.ICV.domein.Kwartetspeler;
import com.ICV.dto.KwartetkaartDto;

@RestController
public class KwartetspelerEndpoint {

	@Autowired
	KwartetspelerService kss;
	@Autowired
	KwartetkaartService kks;
	
	@GetMapping("spelers")
	public Stream<Kwartetspeler> getSpelers(){
		return kss.toonSpelers().stream();
	}
	
	@GetMapping("speler/{spelerid}")
	public Kwartetspeler getSpelerById(Long id) {
		return kss.getSpelerById(id);
	}
	
	@PostMapping("maakSpeler")
	public void maakSpeler(@RequestBody Kwartetspeler ks) {
		kss.save(ks);
	}
	
	@GetMapping("kaartenVanSpeler/{spelerid}")
	public Stream<KwartetkaartDto> kaartenVanSpeler(@PathVariable("spelerid") Long spelerid){
		return kss.getSpelerById(spelerid).getKaarten().stream().map(k -> new KwartetkaartDto(k.getId(), k.getTitel(), k.getBeschrijving()));
	}
}
