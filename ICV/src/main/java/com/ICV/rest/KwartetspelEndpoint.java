package com.ICV.rest;

import static com.ICV.IcvApplication.random;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.KwartetService;
import com.ICV.controller.KwartetkaartService;
import com.ICV.controller.KwartetpotService;
import com.ICV.controller.KwartetspelService;
import com.ICV.controller.KwartetspelerService;
import com.ICV.domein.Kwartetkaart;
import com.ICV.domein.Kwartetpot;
import com.ICV.domein.Kwartetspel;
import com.ICV.domein.Kwartet;
import com.ICV.domein.Kwartetspeler;
import com.ICV.dto.KwartetspelDto;
import com.ICV.dto.KwartetspelerDto;

@RestController
public class KwartetspelEndpoint {
	@Autowired
	KwartetspelService kss;
	@Autowired
	KwartetkaartService kks;
	@Autowired
	KwartetpotService kps;
	@Autowired
	KwartetspelerService spelerService;
	@Autowired
	KwartetService kwartetService;

	@GetMapping("kwartetspellen")
	public Stream<KwartetspelDto> getKwartetspellen(){
		return kss.toonSpellen().stream().map(s -> new KwartetspelDto(s));
	}

	@GetMapping("maakKwartetspel/{naam}")
	public KwartetspelDto maakKwartetspel(@PathVariable("naam") String naam) {
		Kwartetspel ks = new Kwartetspel();
		ks.setNaam(naam);
		kss.save(ks);

		//Maak een nieuwe pot aan
		Kwartetpot p = new Kwartetpot(kks.toonKwartetkaarten());
		ks.setKwartetpot(p);
		p.setKwartetspel(ks);
		kps.save(p);

		List<Kwartetspeler> spelers = new ArrayList<>();
		Kwartetspeler Bezoeker = new Kwartetspeler("jou");
		Kwartetspeler Lisia = new Kwartetspeler("Lisia");
		Kwartetspeler Peter = new Kwartetspeler("Peter");
		spelers.add(Peter);
		spelers.add(Lisia);
		spelers.add(Bezoeker);
		spelerService.saveAll(spelers);
		
		ks.voegSpelersToe(spelers);
		
		ks.deelKaarten();
		ks.setGamelog("");
		kss.save(ks);

		return new KwartetspelDto(ks);
	}

	@GetMapping("spelersVanSpel/{kwartetspelid}")
	public Stream<KwartetspelerDto> spelersVanSpel(@PathVariable("kwartetspelid") Long kwartetspelid) {
		Kwartetspel spel = kss.getKwartetSpelById(kwartetspelid);
		return spel.getKwartetspelers().stream().map(s -> new KwartetspelerDto(s));
	}

	@GetMapping("kwartetspel/{kwartetspelid}")
	public KwartetspelDto getKwartetspelById(@PathVariable("kwartetspelid") Long kwartetspelid) {
		try {
			Kwartetspel spel = kss.getKwartetSpelById(kwartetspelid);
			return new KwartetspelDto(spel);
		} catch(Exception e){
			return new KwartetspelDto();
		}
	}

	//Deze methode wordt aangeroepen met de keuze van een (AI)speler
	@PostMapping("vraagKaart/{kwartetkaartid}/aan/{spelerid}")
	public boolean vraagKaartAan(@RequestBody KwartetspelDto spel, @PathVariable("kwartetkaartid") Long kwartetkaartid, @PathVariable("spelerid") Long spelerid) {
		Kwartetkaart kaart = kks.getKwartetkaartById(kwartetkaartid);
		Kwartetspeler speler = spelerService.getSpelerById(spelerid);
		Kwartetspel kwartetspel = kss.getKwartetSpelById(spel.getId());

		boolean geslaagd = kwartetspel.verwerkenVraag(kaart, speler);

		if(!geslaagd) {
			kwartetspel.clearGameLog();
		}
		spelerService.save(speler);
		kks.save(kaart);
		kss.save(kwartetspel);
		return geslaagd;
	}

	@PostMapping("AIvraagtKaart")
	public void AIvraagtKaart(@RequestBody KwartetspelDto speldto) {
		String log = "";
		Kwartetspel spel = kss.getKwartetSpelById(speldto.getId());

		//Selecteer een willekeurige tegenspeler met kaarten
		int i = spel.getKwartetspelers().indexOf(spel.getActieveSpeler());
		Kwartetspeler vragenVan;
		if(spel.getKwartetspelers().get((i+1)%3).geenKaarten()) {
			vragenVan = spel.getKwartetspelers().get((i+2)%3);
		} else if(spel.getKwartetspelers().get((i+2)%3).geenKaarten()) {
			vragenVan = spel.getKwartetspelers().get((i+1)%3);
		} else {
			if(random.nextBoolean()) {
				vragenVan = spel.getKwartetspelers().get((i+1)%3);
			} else {
				vragenVan = spel.getKwartetspelers().get((i+2)%3);
			}
		}

		if(spel.getActieveSpeler().geenKaarten()) {
			spel.trekRandomKaartVan(vragenVan);
			log +=  spel.getActieveSpeler().getNaam() + "*trekt*" + "een willekeurige kaart" + "*van*" + vragenVan.getNaam() + "*.+"; 
			spel.voegToeAanGameLog(log);
			if(!spel.afgelopen()) {
				AIvraagtKaart(speldto);
			}
		} else {
			//Selecteer een willekeurige kaart
			List<Kwartetkaart> teVragenKaarten = spel.getActieveSpeler().teVragenKaarten();
			Kwartetkaart kaart = teVragenKaarten.get(random.nextInt(teVragenKaarten.size()));

			//De AI mag blijven spelen zolang hij kaarten ontvangt die hij vraagt.
			log +=  spel.getActieveSpeler().getNaam() + "*vraagt*" + kaart.getKwartet().getCategorie() + " - " + kaart.getTitel() + "*aan*" + vragenVan.getNaam() + ","; 

			if(spel.verwerkenVraag(kaart, vragenVan)) {
				log += "*en ontving deze.+";
				spel.voegToeAanGameLog(log);

				kss.save(spel);

				if(!spel.afgelopen()) {
					AIvraagtKaart(speldto);
				}

			} else {
				log += "*maar kreeg deze niet";
				if(spel.getKwartetpot().isLeeg()) {
					log += ".";
				} else {
					log += " en pakte een kaart.";
				}
				if(spel.getActieveSpeler().getNaam().equals("Peter")) {
					log += "+";
				}
				spel.voegToeAanGameLog(log);
				kss.save(spel);
			}
		}
	}

	@PostMapping("trekKaartVan/{spelerid}")
	public void trekKaartVan(@PathVariable("spelerid") Long spelerid, @RequestBody KwartetspelDto speldto) {
		Kwartetspeler a = spelerService.getSpelerById(spelerid);
		Kwartetspel spel = kss.getKwartetSpelById(speldto.getId());
		spel.trekRandomKaartVan(a);
		spelerService.save(a);
		spelerService.save(spel.getActieveSpeler());
		kss.save(spel);
	}

	@GetMapping("volgendeSpeler/{spelid}")
	public KwartetspelDto volgendeSpeler(@PathVariable("spelid") Long spelid) {
		Kwartetspel spel = kss.getKwartetSpelById(spelid);
		spel.volgendeSpeler();
		kss.save(spel);
		return new KwartetspelDto(spel);
	}

	//TODO: Unit testing
	@GetMapping("verwijderSpel/{spelid}")
	public void verwijderSpel(@PathVariable("spelid") Long spelid) {
		Kwartetspel spel = kss.getKwartetSpelById(spelid);
		for(Kwartetspeler s : spel.getKwartetspelers()) {
			for(Kwartet k : s.getKwartetten()) {
				k.verwijderKwartetspeler(s);
				kwartetService.save(k);
			}
		}
		spelerService.deleteAll(spel.getKwartetspelers());
		kps.delete(spel.getKwartetpot());
		//Wanneer een spel afgelopen is, heeft geen enkele speler meer kaarten in zijn handen,
		//dit hoeft dus niet handmatig ontbonden te worden.
		kss.delete(spel);
	}
}
