package com.ICV.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.QuizService;
import com.ICV.controller.QuizvraagService;
import com.ICV.domein.Quiz;

@RestController
public class QuizEndpoint {
	@Autowired
	QuizService service;
	@Autowired
	QuizvraagService vraagService;

	@GetMapping("quiz/{quizid}")
	public Quiz getQuizById(@PathVariable("quizid") Long id) {
		return service.getQuizById(id);
	}

	@GetMapping("toonQuizzes")
	public List<Quiz> toonQuizzes(){
		return service.toonQuizzes();
	}

	@GetMapping("maakQuiz/{naam}")
	public Quiz maakQuiz(@PathVariable("naam") String naam) {
		Quiz q = new Quiz();
		q.setNaam(naam);
		q.setHuidigeVraag(1);
		q.setAantalOvereenkomstig(0);
		q.setAantalVragen(vraagService.aantalVragen());
		service.save(q);
		return q;
	}

	@PostMapping("updateScore/{quizid}")
	public void updateScore(@PathVariable("quizid") Long quizid, @RequestBody Integer goed) {
		Quiz q = service.getQuizById(quizid);
		q.updateAantalOvereenkomstig(goed == 1);
		service.save(q);
	}

	@GetMapping("volgendeVraag/{quizid}")
	public Quiz volgendeVraag(@PathVariable("quizid") Long quizid) {
		Quiz q = service.getQuizById(quizid);
		q.volgendeVraag();
		service.save(q);
		return q;
	}

	@PostMapping("verwijderQuiz")
	public void verwijderQuiz(@RequestBody Quiz quiz) {
		Quiz q = service.getQuizById(quiz.getId());
		service.remove(q);
	}
}
