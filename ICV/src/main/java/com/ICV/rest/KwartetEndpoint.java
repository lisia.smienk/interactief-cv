package com.ICV.rest;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.KwartetService;
import com.ICV.controller.KwartetkaartService;
import com.ICV.controller.KwartetspelerService;
import com.ICV.domein.Kwartet;
import com.ICV.domein.Kwartetkaart;
import com.ICV.domein.Kwartetspeler;
import com.ICV.dto.KwartetDto;
import com.ICV.dto.KwartetKaartKoppelingDto;
import com.ICV.dto.KwartetkaartDto;

@RestController
public class KwartetEndpoint {
	@Autowired
	KwartetService ks;
	@Autowired
	KwartetkaartService kks;
	@Autowired
	KwartetspelerService kss;
	
	@GetMapping("kwartetten")
	public Stream<KwartetDto> getKwartetten(){
		return ks.toonKwartetten().stream().map(k -> new KwartetDto(k));
	}
	
	@GetMapping("kwartet/{kwartetid}")
	public KwartetDto getKwartetById(@PathVariable("kwartetid") Long kwartetid) {
		Kwartet k = ks.getKwartetById(kwartetid);
		return new KwartetDto(k);
	}
	
	@GetMapping("kwartettenVanSpeler/{kwartetspelerid}")
	public Stream<KwartetDto> kwartettenVanSpeler(@PathVariable("kwartetspelerid") Long kwartetspelerid){
		Kwartetspeler s = kss.getSpelerById(kwartetspelerid);
		return s.getKwartetten().stream().map(k -> new KwartetDto(k));
	}
	
	@GetMapping("kaartenVanKwartet/{kwartetid}")
	public Stream<KwartetkaartDto> getKwartetkaartenByKwartetid(@PathVariable("kwartetid") Long kwartetid) {
		Kwartet k = ks.getKwartetById(kwartetid);
		return k.getKaarten().stream().map(kk -> new KwartetkaartDto(kk));
	}
	
	@GetMapping("kwartetvan/{kwartetkaartid}")
	public KwartetDto getKwartetByKwartetKaart(@PathVariable("kwartetkaartid") Long kwartetkaartid) {
		Kwartetkaart kk = kks.getKwartetkaartById(kwartetkaartid);
		Kwartet k = kk.getKwartet();
		return new KwartetDto(k);
	}
	
	@PostMapping("maakKwartet")
	public void maakKwartet(@RequestBody Kwartet k) {
		ks.save(k);
	}
	
	@PostMapping("voegKwartetkaartToe")
	public void voegKwartetkaartToe(@RequestBody KwartetKaartKoppelingDto dto) {
		Kwartet k = ks.getKwartetById(dto.getKwartetid());
		Kwartetkaart kk = kks.getKwartetkaartById(dto.getKwartetkaartid());
		
		k.voegKwartetkaartToe(kk);
		ks.save(k);
		
		kk.setKwartet(k);
		kks.save(kk);
	}
}
