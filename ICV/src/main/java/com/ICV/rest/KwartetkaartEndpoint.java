package com.ICV.rest;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.KwartetService;
import com.ICV.controller.KwartetkaartService;
import com.ICV.controller.KwartetpotService;
import com.ICV.domein.Kwartetkaart;
import com.ICV.dto.KwartetkaartDto;

@RestController
public class KwartetkaartEndpoint {
	
	@Autowired
	KwartetkaartService kks;
	@Autowired
	KwartetService ks;
	@Autowired
	KwartetpotService ps;
	
	@GetMapping("kwartetkaarten")
	public Stream<KwartetkaartDto> getKwartetkaarten(){
		return kks.toonKwartetkaarten().stream().map(k -> new KwartetkaartDto(k));
	}
	
	@GetMapping("kwartetkaart/{kwartetkaartid}")
	public KwartetkaartDto getKwartetkaartById(@PathVariable("kwartetkaartid") Long kwartetkaartid) {
		return new KwartetkaartDto(kks.getKwartetkaartById(kwartetkaartid));
	}
	
	@PostMapping("maakKwartetkaart")
	public void maakKwartetkaart(@RequestBody Kwartetkaart kk) {
		kks.save(kk);
	}
}
