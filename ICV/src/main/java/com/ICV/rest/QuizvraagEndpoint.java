package com.ICV.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.QuizvraagService;
import com.ICV.domein.Quizvraag;


@RestController
public class QuizvraagEndpoint {
	@Autowired
	QuizvraagService service;
	
	@GetMapping("quizvraag/{quizvraagnr}")
	public Quizvraag getQuizvraagByNr(@PathVariable("quizvraagnr") int nr) {
		return service.getQuizvraagByNr(nr);
	}
	
	@PostMapping("maakQuizvraag")
	public void maakQuizvraag(@RequestBody Quizvraag vraag) {
		service.save(vraag);
	}
}
