package com.ICV.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ICV.controller.KwartetpotService;
import com.ICV.controller.KwartetspelService;
import com.ICV.domein.Kwartetpot;
import com.ICV.dto.KwartetpotDto;

@RestController
public class KwartetpotEndpoint {
	
	@Autowired
	KwartetpotService ps;
	@Autowired
	KwartetspelService spelService;
	
	@GetMapping("potVanSpel/{kwartetspelid}")
	public KwartetpotDto potVanSpel(@PathVariable("kwartetspelid") Long kwartetspelid) {
		Kwartetpot pot = ps.getByKwartetspel(spelService.getKwartetSpelById(kwartetspelid));
		return new KwartetpotDto(pot.getId(), pot.getKwartetkaarten().size());
	}
}
