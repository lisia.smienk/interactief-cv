package com.ICV;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcvApplication {

	public final static Random random = new Random();
	
	public static void main(String[] args) {
		SpringApplication.run(IcvApplication.class, args);
	}

}
