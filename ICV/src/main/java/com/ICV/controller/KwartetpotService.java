package com.ICV.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Kwartetpot;
import com.ICV.domein.Kwartetspel;

@Service
@Transactional
public class KwartetpotService {

	@Autowired
	KwartetpotRepository kpr;
	
	public void save(Kwartetpot kp) {
		kpr.save(kp);
	}
	
	public List<Kwartetpot> toonKwartetpotten(){
		return kpr.findAll();
	}
	
	public Kwartetpot getKwartetpotById(Long id) {
		return kpr.findById(id).get();
	}
	
	public Kwartetpot getByKwartetspel(Kwartetspel spel) {
		return kpr.findByKwartetspel(spel);
	}
	
	public void delete(Kwartetpot pot) {
		kpr.delete(pot);
	}
}
