package com.ICV.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Kwartetkaart;

@Service
@Transactional
public class KwartetkaartService {

	@Autowired
	KwartetkaartRepository kkr;
	
	public void save(Kwartetkaart kk) {
		kkr.save(kk);
	}
	
	public List<Kwartetkaart> toonKwartetkaarten(){
		return kkr.findAll();
	}
	
	public Kwartetkaart getKwartetkaartById(Long id) {
		return kkr.findById(id).get();
	}
}
