package com.ICV.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Kwartetspeler;

@Service
@Transactional
public class KwartetspelerService {

	@Autowired
	KwartetspelerRepository ksr;
	
	public void save(Kwartetspeler ks) {
		ksr.save(ks);
	}
	
	public void saveAll(Iterable<Kwartetspeler> spelers) {
		ksr.saveAll(spelers);
	}
	
	public List<Kwartetspeler> toonSpelers(){
		return ksr.findAll();
	}
	
	public Kwartetspeler getSpelerById(Long id) {
		return ksr.findById(id).get();
	}
	
	public void delete(Kwartetspeler ks) {
		ksr.delete(ks);
	}
	
	public void deleteAll(Iterable<Kwartetspeler> spelers) {
		ksr.deleteAll(spelers);
	}
	
}
