package com.ICV.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Kwartetpot;
import com.ICV.domein.Kwartetspel;

public interface KwartetpotRepository extends JpaRepository<Kwartetpot, Long> {
	Kwartetpot findByKwartetspel(Kwartetspel spel);
}
