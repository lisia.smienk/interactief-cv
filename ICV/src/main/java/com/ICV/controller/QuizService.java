package com.ICV.controller;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Quiz;

@Service
@Transactional
public class QuizService {
	@Autowired
	QuizRepository qr;
	
	public void save(Quiz q) {
		qr.save(q);
	}
	
	public List<Quiz> toonQuizzes(){
		return qr.findAll();
	}
	
	public Quiz getQuizById(Long id) {
		return qr.findById(id).get();
	}
	
	public void remove(Quiz q) {
		qr.delete(q);
	}
}
