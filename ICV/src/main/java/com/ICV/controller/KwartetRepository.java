package com.ICV.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Kwartet;

public interface KwartetRepository extends JpaRepository<Kwartet, Long>{

}
