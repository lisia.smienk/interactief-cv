package com.ICV.controller;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Quizvraag;

@Service
@Transactional
public class QuizvraagService {
	@Autowired
	QuizvraagRepository qr;
	
	public void save(Quizvraag q) {
		qr.save(q);
	}
	
	public List<Quizvraag> toonQuizvraagzes(){
		return qr.findAll();
	}
	
	public Quizvraag getQuizvraagById(Long id) {
		return qr.findById(id).get();
	}
	
	public Quizvraag getQuizvraagByNr(int nr) {
		return qr.findByVolgnummer(nr);
	}
	
	public long aantalVragen() {
		return qr.count();
	}
}
