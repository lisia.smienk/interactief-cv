package com.ICV.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Kwartet;

@Service
@Transactional
public class KwartetService {
	
	@Autowired
	KwartetRepository kr;
	
	public void save(Kwartet k) {
		kr.save(k);
	}
	
	public List<Kwartet> toonKwartetten(){
		return kr.findAll();
	}
	
	public Kwartet getKwartetById(Long id) {
		return kr.findById(id).get();
	}
	
}
