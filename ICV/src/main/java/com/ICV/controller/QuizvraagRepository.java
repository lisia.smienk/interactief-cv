package com.ICV.controller;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Quizvraag;

public interface QuizvraagRepository extends JpaRepository<Quizvraag, Long> {
	Quizvraag findByVolgnummer(int i);
	long count();
}
