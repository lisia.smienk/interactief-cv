package com.ICV.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Kwartetkaart;

public interface KwartetkaartRepository extends JpaRepository<Kwartetkaart, Long> {

}
