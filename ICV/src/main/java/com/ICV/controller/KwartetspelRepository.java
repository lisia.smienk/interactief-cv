package com.ICV.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Kwartetspel;

public interface KwartetspelRepository extends JpaRepository<Kwartetspel, Long>{

}
