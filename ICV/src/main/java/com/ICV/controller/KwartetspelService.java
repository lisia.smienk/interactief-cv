package com.ICV.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ICV.domein.Kwartetspel;

@Service
@Transactional
public class KwartetspelService {

	@Autowired
	KwartetspelRepository ksr;
	
	public void save(Kwartetspel ks) {
		ksr.save(ks);
	}
	
	public List<Kwartetspel> toonSpellen(){
		return ksr.findAll();
	}	
	
	public Kwartetspel getKwartetSpelById(Long id) {
		return ksr.findById(id).get();
	}
	
	public void delete(Kwartetspel ks) {
		ksr.delete(ks);
	}
}
