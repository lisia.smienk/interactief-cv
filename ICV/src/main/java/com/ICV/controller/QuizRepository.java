package com.ICV.controller;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ICV.domein.Quiz;

public interface QuizRepository extends JpaRepository<Quiz, Long>{

}
