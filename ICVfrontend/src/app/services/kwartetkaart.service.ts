import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Kwartetkaart } from '../model/Kwartetkaart';
import { Kwartet } from '../model/Kwartet';
import { Kwartetspeler } from '../model/Kwartetspeler';
import { Kwartetspel } from '../model/Kwartetspel';
import { Kwartetpot } from '../model/Kwartetpot';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class KwartetkaartService {

  constructor(private http: HttpClient) { }

  toonKwartetkaarten(): Observable<Kwartetkaart[]> {
    return this.http.get<Kwartetkaart[]>('http://localhost:8082/kwartetkaarten');
  }

  findKwartetkaart(kwartetkaartid: number): Observable<Kwartetkaart> {
    return this.http.get<Kwartetkaart>('http://localhost:8082/kwartetkaart/' + kwartetkaartid);
  }

  findKwartet(kaart: Kwartetkaart): Observable<Kwartet> {
    return this.http.get<Kwartet>('http://localhost:8082/kwartetvan/' + kaart.id);
  }

  findKwartettenVanSpeler(kwartetspelerid: number): Observable<Kwartet[]> {
    return this.http.get<Kwartet[]>('http://localhost:8082/kwartettenVanSpeler/' + kwartetspelerid);
  }

  findKwartetkaartenVanKwartet(kwartetid: number): Observable<Kwartetkaart[]> {
    return this.http.get<Kwartetkaart[]>('http://localhost:8082/kaartenVanKwartet/' + kwartetid);
  }

  findKwartetkaartenVanSpeler(spelerid: number): Observable<Kwartetkaart[]> {
    return this.http.get<Kwartetkaart[]>('http://localhost:8082/kaartenVanSpeler/' + spelerid);
  }

  findSpeler(spelerid: number): Observable<Kwartetspeler> {
    return this.http.get<Kwartetspeler>("http://localhost:8082/speler/" + spelerid);
  }

  findSpellen(): Observable<Kwartetspel[]> {
    return this.http.get<Kwartetspel[]>('http://localhost:8082/kwartetspellen');
  }

  maakNieuwSpel(naam: string): Observable<Kwartetspel> {
    return this.http.get<Kwartetspel>('http://localhost:8082/maakKwartetspel/' + naam);
  }

  findPotVanSpel(spel: Kwartetspel): Observable<Kwartetpot> {
    return this.http.get<Kwartetpot>('http://localhost:8082/potVanSpel/' + spel.id);
  }

  findKwartetspel(kwartetspelid: number): Observable<Kwartetspel> {
    return this.http.get<Kwartetspel>('http://localhost:8082/kwartetspel/' + kwartetspelid);
  }

  findSpelersVanSpel(spel: Kwartetspel): Observable<Kwartetspeler[]> {
    return this.http.get<Kwartetspeler[]>('http://localhost:8082/spelersVanSpel/' + spel.id);
  }

  vraagKaartAan(spel: Kwartetspel, kaartid: number, speler: Kwartetspeler): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8082/vraagKaart/' + kaartid + '/aan/' + speler.id, spel);
  }

  trekKaartVan(spel: Kwartetspel, speler: Kwartetspeler): Observable<void> {
    return this.http.post<void>('http://localhost:8082/trekKaartVan/' + speler.id, spel);
  }

  AIkaartVragen(spel: Kwartetspel): Observable<void>{
    return this.http.post<void>('http://localhost:8082/AIvraagtKaart', spel);
  }

  volgendeSpeler(spelid: number): Observable<Kwartetspel>{
    return this.http.get<Kwartetspel>('http://localhost:8082/volgendeSpeler/' + spelid);
  }

  verwijderSpel(spel: Kwartetspel): Observable<void>{
    return this.http.get<void>('http://localhost:8082/verwijderSpel/' + spel.id);
  }
}