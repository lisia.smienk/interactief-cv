import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Quiz } from '../model/Quiz';
import { Quizvraag } from '../model/Quizvraag';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) { }

  findQuizvraag(quizvraagnr: number): Observable<Quizvraag> {
    return this.http.get<Quizvraag>('http://localhost:8082/quizvraag/' + quizvraagnr);
  }

  findQuiz(quizid: number): Observable<Quiz> {
    return this.http.get<Quiz>('http://localhost:8082/quiz/' + quizid);
  }

  findQuizzes(): Observable<Quiz[]> {
    return this.http.get<Quiz[]>('http://localhost:8082/toonQuizzes');
  }

  maakNieuweQuiz(naam: String): Observable<Quiz> {
    return this.http.get<Quiz>('http://localhost:8082/maakQuiz/' + naam);
  }

  updateScore(nr: number, quizid: number): Observable<void> {
    return this.http.post<void>('http://localhost:8082/updateScore/' + quizid, nr);
  }

  volgendeVraag(quizid: number): Observable<Quiz>{
    return this.http.get<Quiz>('http://localhost:8082/volgendeVraag/' + quizid);
  }

  verwijderQuiz(quiz: Quiz): Observable<void>{
    return this.http.post<void>('http://localhost:8082/verwijderQuiz', quiz);
  }
}
