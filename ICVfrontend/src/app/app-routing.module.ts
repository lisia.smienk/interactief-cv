import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KwartetkaartComponent } from '../app/components/kwartetkaart/kwartetkaart.component';

import { HomepageComponent } from './pages/homepage/homepage.component';
import { KwartetpageComponent } from './pages/kwartetpage/kwartetpage.component';
import { ContactpageComponent } from './pages/contactpage/contactpage.component';
import { KwartethandComponent } from './components/kwartethand/kwartethand.component';
import { KwartetmenupageComponent } from './pages/kwartetmenupage/kwartetmenupage.component';
import { QuizmenupageComponent} from './pages/quizmenupage/quizmenupage.component';
import { QuizpageComponent } from './pages/quizpage/quizpage.component';

const routes: Routes = [
  {path: "", component: HomepageComponent, pathMatch: "full"},
  {path: "kwartet", component: KwartetmenupageComponent},
  {path: "kwartet/:kwartetspelid", component: KwartetpageComponent},
  {path: "quiz", component: QuizmenupageComponent},
  {path: "quiz/:quizid", component: QuizpageComponent},
  {path: "contact", component: ContactpageComponent},
  {path: "kwartetkaart/:kwartetkaartid", component: KwartetkaartComponent},
  {path: "kwartethand", component: KwartethandComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }