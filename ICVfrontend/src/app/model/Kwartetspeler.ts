export class Kwartetspeler{
    id: number;
    naam: string;
    aantalKaarten: number;
    aantalKwartetten: number;
}