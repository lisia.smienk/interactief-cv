import { Kwartetspeler } from "./Kwartetspeler";

export class Kwartetspel{
    id: number;
    naam: string;
    actieveSpeler: Kwartetspeler;
    gamelog: string;
    status: Boolean;
}