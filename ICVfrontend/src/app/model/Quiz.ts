export class Quiz{
    id: number;
    huidigeVraag: number;
    naam: String;
    aantalOvereenkomstig: number;
    aantalVragen: number;
}