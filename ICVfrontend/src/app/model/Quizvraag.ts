export class Quizvraag {
    id: number;
    volgnummer: number;
    vraag: String;
    antwoorden: String[];
    indexLisia: number;
}