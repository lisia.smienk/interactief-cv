import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { KwartetkaartComponent } from './components/kwartetkaart/kwartetkaart.component';
import { KwartetkaartService } from './services/kwartetkaart.service';
import { KwartetpageComponent } from './pages/kwartetpage/kwartetpage.component';
import { ContactpageComponent } from './pages/contactpage/contactpage.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { KwartethandComponent } from './components/kwartethand/kwartethand.component';
import { KwartetmenupageComponent } from './pages/kwartetmenupage/kwartetmenupage.component';
import { KwartetuitlegComponent } from './components/kwartetuitleg/kwartetuitleg.component';
import { KwartetpotComponent } from './components/kwartetpot/kwartetpot.component';
import { KwartetgamelogComponent } from './components/kwartetgamelog/kwartetgamelog.component';
import { KwartettenComponent } from './components/kwartetten/kwartetten.component';
import { TegenstanderComponent } from './components/tegenstander/tegenstander.component';
import { LegeKaartComponent } from './components/lege-kaart/lege-kaart.component';
import { QuizmenupageComponent } from './pages/quizmenupage/quizmenupage.component';
import { QuizpageComponent } from './pages/quizpage/quizpage.component';
import { QuizkaartjeComponent } from './components/quizkaartje/quizkaartje.component';
import { QuizscoreComponent } from './components/quizscore/quizscore.component';

@NgModule({
  declarations: [
    AppComponent,
    KwartetkaartComponent,
    KwartetpageComponent,
    ContactpageComponent,
    HomepageComponent,
    HeaderComponent,
    FooterComponent,
    KwartethandComponent,
    KwartetmenupageComponent,
    KwartetuitlegComponent,
    KwartetpotComponent,
    KwartetgamelogComponent,
    KwartettenComponent,
    TegenstanderComponent,
    LegeKaartComponent,
    QuizmenupageComponent,
    QuizpageComponent,
    QuizkaartjeComponent,
    QuizscoreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    KwartetkaartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
