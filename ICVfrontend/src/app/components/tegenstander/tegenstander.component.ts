import { Component, OnInit, Input } from '@angular/core';
import { Kwartetspeler } from '../../model/Kwartetspeler';

@Component({
  selector: 'app-tegenstander',
  templateUrl: './tegenstander.component.html',
  styleUrls: ['./tegenstander.component.css']
})
export class TegenstanderComponent implements OnInit {

  foto: string = "een_kaart.jpg";

  constructor() { }

  @Input() speler: Kwartetspeler;

  ngOnInit(): void {
    if (this.speler != null) {
      if (this.speler.naam == "Peter") {
        switch (this.speler.aantalKaarten) {
          case 0: this.foto = "p_geen_kaart.jpg"; break;
          case 1: this.foto = "p_een_kaart.jpg"; break;
          case 2: this.foto = "p_twee_kaarten.jpg"; break;
          case 3: this.foto = "p_drie_kaarten.jpg"; break;
          case 4: this.foto = "p_vier_kaarten.jpg"; break;
          case 5: this.foto = "p_vijf_kaarten.jpg"; break;
          case 6: this.foto = "p_zes_kaarten.jpg"; break;
          case 7: this.foto = "p_zeven_kaarten.jpg"; break;
          case 8: this.foto = "p_acht_kaarten.jpg"; break;
          default: this.foto = "p_acht_kaarten.jpg"; break;  
        }
      } else {
        switch (this.speler.aantalKaarten) {
          case 0: this.foto = "geen_kaarten.jpg"; break;
          case 1: this.foto = "een_kaart.jpg"; break;
          case 2: this.foto = "twee_kaarten.jpg"; break;
          case 3: this.foto = "drie_kaarten.jpg"; break;
          case 4: this.foto = "vier_kaarten.jpg"; break;
          case 5: this.foto = "vijf_kaarten.jpg"; break;
          case 6: this.foto = "zes_kaarten.jpg"; break;
          case 7: this.foto = "zeven_kaarten.jpg"; break;
          case 8: this.foto = "acht_kaarten.jpg"; break;
          default: this.foto = "acht_kaarten.jpg"; break;
        }
      }
    }
  }

  ngOnChanges() {
    this.ngOnInit();
  }
}