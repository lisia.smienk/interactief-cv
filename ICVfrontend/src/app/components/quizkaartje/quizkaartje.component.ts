import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Quizvraag } from '../../model/Quizvraag';
import { QuizService } from '../../services/quiz.service';

@Component({
  selector: 'app-quizkaartje',
  templateUrl: './quizkaartje.component.html',
  styleUrls: ['./quizkaartje.component.css']
})
export class QuizkaartjeComponent implements OnInit {

  vraag: Quizvraag;

  constructor(private service: QuizService, private activatedrouter: ActivatedRoute) { }

  @Input()
  quizvraagnr: number = null;

  ngOnInit(): void {
    if (this.quizvraagnr != null) {
      //TODO: HAAL DEZE WAARDE UIT DE DATABASE
      if (this.quizvraagnr < 4) {
        this.service.findQuizvraag(this.quizvraagnr).subscribe(vraag => {
          this.vraag = vraag;
        })
        document.getElementById("score").style.display = 'none';
        document.getElementById("kaartje").style.display = 'block';
      } else {
        //Toon eindscore
        document.getElementById("kaartje").style.display = 'none';
        document.getElementById("score").style.display = 'block';
        document.getElementById("volgendebtn").style.display = 'none';
      }
    }
  }

  ngOnChanges() {
    this.ngOnInit();
    var ids = ["A", "B", "C", "D"];
    for (let item of ids.values()) {
      document.getElementById(item).style.color = 'black';
      document.getElementById(item).style.fontWeight = 'normal';
      document.getElementById(item).style.border = 'none';
      (document.getElementById('volgendebtn') as HTMLInputElement).disabled = true;
    }
  }

  kiesAntwoord(nr: number) {
    if ((document.getElementById('volgendebtn') as HTMLInputElement).disabled) {
      document.getElementById(this.intToLetter(nr)).style.color = '#ff6666';
      document.getElementById(this.intToLetter(nr)).style.fontWeight = 'bold';
      document.getElementById(this.intToLetter(nr)).style.border = 'solid 1px #ff6666';
      document.getElementById(this.intToLetter(this.vraag.indexLisia)).style.fontWeight = 'bold';
      (document.getElementById('volgendebtn') as HTMLInputElement).disabled = false;
      this.activatedrouter.paramMap.subscribe(params => {
        if (nr == this.vraag.indexLisia) {
          this.service.updateScore(1, Number.parseInt(params.get("quizid"))).subscribe();
        } else {
          this.service.updateScore(0, Number.parseInt(params.get("quizid"))).subscribe();
        }
      })
    }
  }

  private intToLetter(nr: number) {
    switch (nr) {
      case 1: return "A";
      case 2: return "B";
      case 3: return "C";
      case 4: return "D";
    }
  }
}
