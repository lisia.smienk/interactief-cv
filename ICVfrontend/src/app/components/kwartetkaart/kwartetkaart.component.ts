import { Component, OnInit, Input } from '@angular/core';
import { KwartetkaartService } from '../../services/kwartetkaart.service';
import { Router } from '@angular/router';
import { Kwartetkaart } from '../../model/Kwartetkaart';
import { Kwartet } from '../../model/Kwartet';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kwartetkaart',
  templateUrl: './kwartetkaart.component.html',
  styleUrls: ['./kwartetkaart.component.css']
})

export class KwartetkaartComponent implements OnInit {

  kwartetkaarten: Array<Kwartetkaart> = new Array(4);
  kwartetkaart: Kwartetkaart;
  kwartet: Kwartet = {id: null, categorie: null, kleur: "white"};
  kleur: String;

  constructor(private ks: KwartetkaartService, private router: Router, private activatedrouter: ActivatedRoute) { }

  @Input()
  kwartetkaartid: number;

  ngOnInit(): void {
    this.ks.findKwartetkaart(this.kwartetkaartid).subscribe(kaart => {
      this.kwartetkaart = kaart;
      this.ks.findKwartet(kaart).subscribe(kwartet => {
        this.kwartet = kwartet;
        this.ks.findKwartetkaartenVanKwartet(kwartet.id).subscribe(kaarten => {
          this.kwartetkaarten = kaarten;
        })
      })
    })
  }

  vraagKaart(k: Kwartetkaart) {
    document.getElementById("gevraagdeKaart").innerHTML = "Kaart: " + k.titel;
    document.getElementById("gevraagdeKaart").title = "" + k.id;
  }
}