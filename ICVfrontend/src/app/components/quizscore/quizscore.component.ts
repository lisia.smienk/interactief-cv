import { Component, Input, OnInit } from '@angular/core';
import { Quiz } from '../../model/Quiz';

@Component({
  selector: 'app-quizscore',
  templateUrl: './quizscore.component.html',
  styleUrls: ['./quizscore.component.css']
})
export class QuizscoreComponent implements OnInit {

  percentage: string;

  constructor() { }

  @Input()
  quiz: Quiz = null;

  ngOnInit(){
    if(this.quiz != null){
      this.percentage = (this.quiz.aantalOvereenkomstig/this.quiz.aantalVragen*100).toFixed(1);
      var p = Number.parseFloat(this.percentage);
      if(p < 25){
        document.getElementById("scoreTekst").innerHTML = 
        "Jij en Lisia hebben blijkbaar veel verschillen. Maar dat geeft niet. Je weet wat ze zeggen: tegenpolen trekken elkaar aan. Ja toch? Misschien kunnen jullie veel van elkaar leren, door je verschillende visie en ervaringen met elkaar te delen.";
      } else if(p < 50){

      } else if(p < 75){
        "Wauw! Wat veel overeenkomstige antwoorden! Jullie .."; //TODO: Teksten afmaken
      } else {

      }
    }
  }

  ngOnChanges(){
    this.ngOnInit();
  }
}