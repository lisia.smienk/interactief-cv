import { Component, Input, OnInit } from '@angular/core';
import { Kwartet } from '../../model/Kwartet';
import { KwartetkaartService } from '../../services/kwartetkaart.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kwartetten',
  templateUrl: './kwartetten.component.html',
  styleUrls: ['./kwartetten.component.css']
})
export class KwartettenComponent implements OnInit {

  kwartetten: Kwartet[];

  constructor(private ks: KwartetkaartService, private router: Router, private activatedrouter: ActivatedRoute) { }

  @Input()
  kwartetspelerid: number = null;

  ngOnInit(): void {
    if (this.kwartetspelerid != null) {
      this.ks.findKwartettenVanSpeler(this.kwartetspelerid).subscribe(kwartetten => {
        this.kwartetten = kwartetten;
        this.toonKwartetten();
      })
    }
  }

  ngOnChanges() {
    this.ngOnInit();
  }

  toonKwartetten(){
    document.getElementById("lijstje").innerHTML = "<b>(" + this.kwartetten.length + ") </b>";
    for(let k of this.kwartetten){
      //TODO: Kleur toevoegen
      document.getElementById("lijstje").innerHTML += k.categorie + ", ";
    }
    var string = document.getElementById("lijstje").innerHTML;
    if(this.kwartetten.length > 0){
          document.getElementById("lijstje").innerHTML = string.substring(0, string.length-2);
    }
  }
}
