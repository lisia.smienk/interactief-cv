import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-kwartetgamelog',
  templateUrl: './kwartetgamelog.component.html',
  styleUrls: ['./kwartetgamelog.component.css']
})
export class KwartetgamelogComponent implements OnInit {

  constructor() { }

  @Input()
  log: string;

  ngOnInit(): void {
    if (this.log != null) {
      document.getElementById("gamelog1").innerHTML = "";
      let logs = this.log.split("+");
      for (let i = 0; i < logs.length; i++) {
        let kolommen = logs[i].split("*");
        document.getElementById("gamelog1").innerHTML += "<tr><td style=\"font-weight: 550 \">" + kolommen[0] + "</td><td>" + kolommen[1] + "</td><td style=\"text-align:center\"><b>" + kolommen[2] + "</b></td><td>" + kolommen[3] + "</td><td style=\"text-align:center; font-weight: 550 \">" + kolommen[4] + "</td><td>" + kolommen[5] + "</td></tr>";
      }
    }
  }

  ngOnChanges() {
    this.ngOnInit();
  }
}
