import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-kwartetpot',
  templateUrl: './kwartetpot.component.html',
  styleUrls: ['./kwartetpot.component.css']
})
export class KwartetpotComponent implements OnInit {

  constructor() { }

  @Input()
  aantalKaarten: number;

  ngOnInit(): void {
    if (this.aantalKaarten != null) {
      if (this.aantalKaarten == 0) {
        document.getElementById("status").innerHTML = "-- LEEG --";
      }
    }
  }

  ngOnChanges(){
    this.ngOnInit();
  }
}
