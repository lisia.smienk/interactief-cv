import { Component, Input, OnInit } from '@angular/core';
import { Kwartetkaart } from '../../model/Kwartetkaart';
import { KwartetkaartService } from '../../services/kwartetkaart.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kwartethand',
  templateUrl: './kwartethand.component.html',
  styleUrls: ['./kwartethand.component.css']
})
export class KwartethandComponent implements OnInit {

  handkaarten: Kwartetkaart[];
  kaartids: Array<number>;

  constructor(private ks: KwartetkaartService, private router: Router, private activatedrouter: ActivatedRoute) { }

  @Input()
  kwartetspelerid: number = null;

  ngOnInit(): void {
    if (this.kwartetspelerid != null) {
      this.ks.findKwartetkaartenVanSpeler(this.kwartetspelerid).subscribe(kaarten => {
        this.handkaarten = kaarten;
        this.kaartids = new Array(this.handkaarten.length);
        for (let i = 0; i < this.handkaarten.length; i++) {
          this.kaartids[i] = this.handkaarten[i].id;
        }
        if (kaarten.length > 0) {
          document.getElementById("legeHand").innerHTML = "";
        }
      })
    }
  }

  ngOnChanges() {
    this.ngOnInit();
  }

}
