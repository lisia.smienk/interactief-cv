import { Component, OnInit } from '@angular/core';
import { QuizService } from '../../services/quiz.service';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from '../../model/Quiz';
import { Router } from '@angular/router'

@Component({
  selector: 'app-quizpage',
  templateUrl: './quizpage.component.html',
  styleUrls: ['./quizpage.component.css']
})
export class QuizpageComponent implements OnInit {

  quiz: Quiz;
  vraagnr: number;
  goed: number;

  constructor(private service: QuizService, private activatedrouter: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.activatedrouter.paramMap.subscribe(params => {
      this.service.findQuiz(Number.parseInt(params.get("quizid"))).subscribe(
        quiz => {
          this.quiz = quiz;
          this.vraagnr = quiz.huidigeVraag;
          this.goed = quiz.aantalOvereenkomstig;
          if (quiz.huidigeVraag > quiz.aantalVragen) {
            this.service.verwijderQuiz(quiz).subscribe();
          }
        },
        (error) => {
          //Quiz is niet gevonden in database, toon bericht aan gebruiker.
          document.getElementById("verwijderd").style.display = 'block';
          document.getElementById("score").style.display = 'none';
          document.getElementById("kaartje").style.display = 'none';
        })
    })
  }

  volgendeVraag() {
    this.service.volgendeVraag(this.quiz.id).subscribe(quiz => {
      this.ngOnInit();
    });
  }

  homepage() {
    this.router.navigateByUrl('');
  }
}