import { Component, OnInit } from '@angular/core';
import { KwartetkaartService } from '../../services/kwartetkaart.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Kwartetkaart } from '../../model/Kwartetkaart';
import { Kwartetspeler } from '../../model/Kwartetspeler';
import { Kwartetspel } from '../../model/Kwartetspel';
import { identifierModuleUrl } from '@angular/compiler';
import { Kwartetpot } from '../../model/Kwartetpot';

@Component({
  selector: 'app-kwartetpage',
  templateUrl: './kwartetpage.component.html',
  styleUrls: ['./kwartetpage.component.css']
})
export class KwartetpageComponent implements OnInit {

  kwartetspel: Kwartetspel = { id: null, naam: null, actieveSpeler: null, gamelog: null, status: null };
  kwartetpot: Kwartetpot = { id: null, size: null };

  speler1: Kwartetspeler = { id: null, aantalKaarten: null, aantalKwartetten: null, naam: null };
  speler2: Kwartetspeler = { id: null, aantalKaarten: null, aantalKwartetten: null, naam: null };
  fysiekeSpeler: Kwartetspeler;
  //ID van fysieke speler voor inputveld Kwartethand
  kwartetspelerid: number;
  sizePot: number;

  gamelog: String;

  teVragenKaart: Kwartetkaart;
  tebevragenSpeler: Kwartetspeler;

  verwijderd: boolean = false;

  constructor(private ks: KwartetkaartService, private router: Router, private activatedrouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedrouter.paramMap.subscribe(params => {
      this.ks.findKwartetspel(Number.parseInt(params.get("kwartetspelid"))).subscribe(spel => {
        if (spel.id == null) {
          //Redirect naar kwartetmenupage
          this.router.navigateByUrl('kwartet');
        } else {
          this.kwartetspel = spel;

          if (spel.gamelog != null && spel.gamelog != "") {
            this.gamelog = spel.gamelog;
          }

          this.ks.findPotVanSpel(spel).subscribe(pot => {
            this.kwartetpot = pot;
            this.sizePot = pot.size;
          })
          this.vindSpelers();
        }
      })
    })
  }

  vindSpelers() {
    //Vind alle spelers van het spel en sla ze op
    this.ks.findSpelersVanSpel(this.kwartetspel).subscribe(spelers => {
      for (let i = 0; i < 3; i++) {
        if (spelers[i].naam == "Lisia") {
          this.speler1 = spelers[i];
        } else if (spelers[i].naam == "Peter") {
          this.speler2 = spelers[i];
        } else {
          this.fysiekeSpeler = spelers[i];
          this.kwartetspelerid = this.fysiekeSpeler.id;
        }
        if (this.kwartetspel.status) {
          if (!this.verwijderd) {
            alert("spel verwijderen?");
            this.ks.verwijderSpel(this.kwartetspel).subscribe();
            this.verwijderd = true;
          }
        }
      }
    })
  }

  vraagKaart() {
    let kaartid = document.getElementById("gevraagdeKaart").title;
    if(this.tebevragenSpeler == null){
      alert("Selecter de speler waarvan je een kaart wilt vragen.");
    } else {

    if (kaartid == "") {
      if (this.fysiekeSpeler.aantalKaarten != 0) {
        alert("Selecteer welke kaart je wilt vragen.");
      } else {
        this.ks.trekKaartVan(this.kwartetspel, this.tebevragenSpeler).subscribe(geslaagd => {
          window.location.reload();
        })
      }
    } else {
      this.ks.vraagKaartAan(this.kwartetspel, Number.parseInt(kaartid), this.tebevragenSpeler).subscribe(geslaagd => {
        if (!geslaagd) {
          this.ks.findKwartetspel(this.kwartetspel.id).subscribe(spel => {
            this.kwartetspel = spel;
            if (!spel.status) {
              //Lisia speelt een beurt
              this.ks.AIkaartVragen(this.kwartetspel).subscribe(gamelog => {
                this.ks.findKwartetspel(this.kwartetspel.id).subscribe(spel => {
                  this.kwartetspel = spel;
                  if (!spel.status) {
                    //Peter speelt een beurt
                    this.ks.AIkaartVragen(this.kwartetspel).subscribe(gamelog => {
                      window.location.reload();
                    })
                  } else {
                    window.location.reload();
                  }
                })
              })
            }
          })
        } else {
          window.location.reload();
        }
      })
    }
          
  }
  }

  kiesSpeler(spelerid: number) {
    if (spelerid == 1) {
      document.getElementById("bevraagdeSpeler").innerHTML = "Aan: Lisia";
      this.tebevragenSpeler = this.speler1;
    } else {
      document.getElementById("bevraagdeSpeler").innerHTML = "Aan: Peter";
      this.tebevragenSpeler = this.speler2;
    }
  }

  //TODO: Op den duur verwijderen
  volgendeSpeler() {
    this.ks.volgendeSpeler(this.kwartetspel.id).subscribe(spel => {
      this.kwartetspel = spel;
      window.location.reload();
    });
  }

  ngOnChanges() {
    this.ngOnInit();
  }

}