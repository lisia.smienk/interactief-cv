import { Component, OnInit } from '@angular/core';
import { Kwartetspel } from '../../model/Kwartetspel';
import { KwartetkaartService } from '../../services/kwartetkaart.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kwartetmenupage',
  templateUrl: './kwartetmenupage.component.html',
  styleUrls: ['./kwartetmenupage.component.css']
})
export class KwartetmenupageComponent implements OnInit {

  spellen: Kwartetspel[] = null;

  constructor(private ks: KwartetkaartService, private router: Router, private activatedrouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.ks.findSpellen().subscribe(spellen => {
      this.spellen = spellen;
    })
  }

  laadSpel(spelid: number) {
    this.router.navigateByUrl("kwartet/" + spelid);
  }

  nieuwSpelStarten() {
    var naam = (<HTMLInputElement>document.getElementById("naam")).value;
    if (naam == "") {
      alert("Voer een naam in om een nieuw spel te starten.");
    } else {
      this.ks.maakNieuwSpel(naam).subscribe(k => {
        this.router.navigateByUrl("kwartet/" + k.id);
      });
    }
  }
}
