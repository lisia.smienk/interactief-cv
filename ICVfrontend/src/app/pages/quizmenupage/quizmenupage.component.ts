import { Component, OnInit } from '@angular/core';
import { QuizService } from '../../services/quiz.service';
import { Quiz } from '../../model/Quiz';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quizmenupage',
  templateUrl: './quizmenupage.component.html',
  styleUrls: ['./quizmenupage.component.css']
})
export class QuizmenupageComponent implements OnInit {

  quizzes: Quiz[]; //=null;

  constructor(private service: QuizService, private router: Router, private activatedrouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.service.findQuizzes().subscribe(quizzes => {
      this.quizzes = quizzes;
    })
  }

  laadSpel(quizid: number) {
    this.router.navigateByUrl("quiz/" + quizid);
  }

  nieuwSpelStarten(){
    var naam = (<HTMLInputElement>document.getElementById("naam")).value;
    if (naam == "") {
      alert("Voer een naam in om een nieuw spel te starten.");
    } else {
      this.service.maakNieuweQuiz(naam).subscribe(q => {
        this.router.navigateByUrl("quiz/" + q.id);
      });
    }
  }
}
